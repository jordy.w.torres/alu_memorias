--------------------------------------------------------------------------------
-- Create Date:   14:35:26 06/19/2022
-- Design Name:   
-- Module Name:   C:/Users/K.G/Documents/ProgramasIse/EjerciciosVideos/TB_ALU_8BITS.vhd
-- Project Name:  EjerciciosVideos
-- Karen Brigith Gonzaga Rivas
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Alu_8bits IS
END Alu_8bits;
 
ARCHITECTURE behavior OF Alu_8bits IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Alu_8bits
    PORT(
         a : IN  std_logic_vector(7 downto 0);
         b : IN  std_logic_vector(7 downto 0);
         opcode : IN  std_logic_vector(1 downto 0);
         d : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;

   --Inputs
   signal a_s : std_logic_vector(7 downto 0) ;
   signal b_s : std_logic_vector(7 downto 0) ;
   signal opcode_s : std_logic_vector(1 downto 0);

 	--Outputs
   signal d_s : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
BEGIN

	UUT: Alu_8bits port map(a_s, b_s, opcode_s, d_s);
	a_s <= "00010001", "01100101" after 5 ns;
	b_s <= "00100011", "00000010" after 5 ns, "00111001" after 20 ns;
	opcode_s <= "00", "01" after 5 ns;
 
END;