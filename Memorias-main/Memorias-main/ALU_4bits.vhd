----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:42:17 06/19/2022 
-- Design Name: 
-- Module Name:    ALU_4bits - Behavioral 
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_signed.ALL;
--USE IEEE.std_logic_unsigned.all;

ENTITY ALU_4bitsIS
END ALU_4bits;
 
ARCHITECTURE behavior OF ALU_4bitsIS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ALU_4bits
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         opcode : IN  std_logic_vector(2 downto 0);
         Cout : OUT  std_logic;
         Yout : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := "1011";
   signal b : std_logic_vector(3 downto 0) := "1001";
   signal opcode : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal Cout : std_logic;
   signal Resul : std_logic_vector(3 downto 0) := (others => '0');
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ALU_4bits PORT MAP (a,b,opcode,Cout,Resul);
	-- Generacion de Estimulos --
	a <= "1100" after 80 ns;
	b <= "1010" after 80 ns;
	opcode <= opcode + '1' after 10 ns;
END;